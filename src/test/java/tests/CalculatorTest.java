package tests;

import objects.CalcObjectPage;
import org.testng.annotations.*;
import org.testng.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;


public class CalculatorTest {

        private static CalcObjectPage calc;
        private static WebDriver driver;

    @BeforeClass
    public static void setup() {
        driver = new ChromeDriver();
        calc = new CalcObjectPage(driver);
        calc.open();
    }

    @BeforeMethod
    public void cleanup() {calc.clear();}

    @Test(dataProvider="CalculatorTestData")
    public void DataDrivenTest( String expression, String expected){
    Assert.assertEquals(calc.calculate(expression),expected);
    }

    @DataProvider(name="CalculatorTestData")
       public Object [][] dataTest() {
        return new Object[][]{
                {"0+999999999", "999999999"},
                {"0.0000001-0", "0.0000001"},
                {"10/3", "3.333333333333333"},
                {"1-100000.00", "-99999"},
                {"-6666+15968", "9302"},
                {"555x555", "308025"},
                {"-125-5", "-130"},
                {"0.123x2.56", "0.31488"},
                {"1000000/-0.111", "-9009009.00900901"}
        };
    }

    @AfterClass
    public static void stop() {
        driver.quit();
    }
}
